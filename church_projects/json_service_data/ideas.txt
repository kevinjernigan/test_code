- Store service information in a json file that can be searched
- Search results should return all matching items (For example, if searching based on Speaker "John Doe", all
    results containing John Doe should be returned)
- Would like to have the returned search results be the actual audio/video files, and would like for them to be
    placed into a generated "export" folder.