setlocal ENABLEDELAYEDEXPANSION

set out_path=%1

:: IMPORTANT!!!!
:: Set the path to the VLC installation on your system.
:: Make sure you see the file "vlc.exe"
::
set vlc_path=C:\Program Files\VideoLAN\VLC\vlc

:: Add a trailing backslash if necessary..
:: if defined out_path if not "%out_path:~-1%"=="\" set out_path="%out_path%\"

:: Create the output directory, if necessary..
if not exist %out_path% (
echo Creating output directory..
mkdir %out_path%
) else (
echo Output directory already exists, reusing it..
)

echo Exporting to %out_path% ..

SET /a x=0

:: IMPORTANT!!!!
:: E: represents your CD player's letter drive. You may need to change this to D:
:: FOR /R E:\ %%G IN (*.cda) DO (CALL _sub-vlc.bat "%%G") &
FOR /R E:\ %%G IN (*.cda) DO (CALL :SUB_VLC "%%G")
GOTO :eof

:SUB_VLC
call SET /a x=x+1
ECHO.
ECHO Transcoding %1
set out_path=%out_path:"=%
SET out_track="%out_path%Track!x!.mp3"
echo %out_track%
echo OUT TRACK
ECHO Output to %out_track%
ECHO exporting .. ..

:: Here's where the actual transcoding/conversion happens. The next line
:: fires off a command to VLC.exe with the relevant arguments:
:: IMPORTANT!!!!
:: E: represents your CD player's letter drive. You may need to change this to D:
:: START "" "%vlc_path%" -I http cdda:///E:/ --cdda-track=!x! :sout=#transcode{vcodec=none,acodec=mp3,ab=128,channels=2,samplerate=44100}:std{access="file",mux=raw,dst="%out_track%"} vlc://quit
CALL "%vlc_path%" -I http cdda:///E:/ --cdda-track=!x! :sout=#transcode{vcodec=none,acodec=mp3,ab=128,channels=2,samplerate=44100}:std{access="file",mux=raw,dst=%out_track%} vlc://quit

:eof
