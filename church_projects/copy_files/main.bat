:: PATHS
:: Google Drive		C:\Users\oakla\Google Drive
:: SD Card Video	G:\MP_ROOT\100ANV01 ::: Need to get *.mp4
set googledrive=C:\Users\oakla\Google Drive
set base=C:\Users\oakla\Documents\Oaklawn Baptist Services
set sd=G:\MP_ROOT\100ANV01\*

:: Get date and format as string
for /f "tokens=1-4 delims=/ " %%i in ("%date%") do (
     set dow=%%i
     set month=%%j
     set day=%%k
     set year=%%l
)
set datestr=%month%_%day%_%year%

:: Choose "SM" or "SN" based on time of run
set ctime=%time:~0,5%
if %ctime:~0,2% geq 14 set service=SN
if %ctime:~0,2% leq 14 set service=SM

:: Create root & sub directories
set rootdir=%base%\%service%_%datestr%
set audio=%rootdir%\audio\
set video=%rootdir%\video\

:: If directories already exist, print error; If not, create them.
if exist "%rootdir%" msg * "Directory already exists for this service; Delete and try again."
if not exist "%rootdir%\" MKDIR "%rootdir%"
if not exist "%audio%" MKDIR "%audio%"
if not exist "%video%" MKDIR "%video%"

:: Rip audio files from CD to rootdir/audio
call C:\scripts\_rip-cd.bat "%audio%"

:: Copy video files from SD to rootdir/video
xcopy /s /i /v %sd% %base%\%video%

:: Copy files from localhost to Google Drive
xcopy /s /i /v %base%\%rootdir%\* %googledrive%\%rootdir%\
